package com.odianyun.internship.constant;

/**
 * @description:
 * @author: EDZ
 * @time: 14:27
 * @date: 2021/7/19
 */
public class CommonConstant {

    public static final Integer YES = 1;
    public static final Integer NO = 0;

    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";
}
