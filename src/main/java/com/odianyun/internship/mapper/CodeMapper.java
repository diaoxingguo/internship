package com.odianyun.internship.mapper;

import com.odianyun.internship.model.VO.CodeVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 20:10
 * @date: 2021/7/21
 */
public interface CodeMapper {
    List<CodeVO> listByCategory(@Param("pool") String pool, @Param("category") String category);
}
