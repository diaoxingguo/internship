package com.odianyun.internship.mapper;

import com.odianyun.internship.model.DTO.UUserIdentityDTO;
import com.odianyun.internship.model.PO.UUserIdentityPO;

/**
 * @description:
 * @author: EDZ
 * @time: 12:23
 * @date: 2021/7/22
 */
public interface UUserIdentityMapper {
    String getUserLabel(Long userId);

    int updateUserLabel(UUserIdentityPO po);
}
