package com.odianyun.internship.model.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: EDZ
 * @time: 17:51
 * @date: 2021/7/25
 */
@ApiModel(value = "ArticleDTO",description = "文章入参类")
public class ArticleDTO implements Serializable {

    private static final long serialVersionUID = 1949359827930840632L;
    @ApiModelProperty(value = "标题", example = "这是文章标题", required = true)
    private String title;
    @ApiModelProperty(value = "作者", example = "兴国", required = true)
    private String author;
    @ApiModelProperty(hidden= true)
    private Date createTime;
    @ApiModelProperty(hidden= true)
    private Integer viewNum;
    @ApiModelProperty(value = "文章内容", example = "这是文章内容", required = true)
    private String content;
    @ApiModelProperty(value = "用户id", example = "123456", required = true)
    private Long userId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
