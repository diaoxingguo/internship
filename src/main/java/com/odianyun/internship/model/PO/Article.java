package com.odianyun.internship.model.PO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 18:28
 * @date: 2021/7/25
 */
@ApiModel(value = "文章信息类")
public class Article implements Serializable {
    private static final long serialVersionUID = 9023438031030784713L;

    @ApiModelProperty("文章id")
    @Id
    private String id;
    @ApiModelProperty("文章标题")
    private String title;
    @ApiModelProperty("文章作者")
    private String author;
    private Date createTime;
    private Integer viewNum;
    private String content;
    private Long userId;

    @ApiModelProperty("点赞列表")
    private List<LikeInfo> likeInfo;
    private List<CommentInfo> commentInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<LikeInfo> getLikeInfo() {
        return likeInfo;
    }

    public void setLikeInfo(List<LikeInfo> likeInfo) {
        this.likeInfo = likeInfo;
    }

    public List<CommentInfo> getCommentInfo() {
        return commentInfo;
    }

    public void setCommentInfo(List<CommentInfo> commentInfo) {
        this.commentInfo = commentInfo;
    }
}
