package com.odianyun.internship.model.PO;

import org.springframework.data.annotation.Transient;

import java.io.Serializable;

/**
 * @description:
 * @author: EDZ
 * @time: 16:24
 * @date: 2021/7/23
 */
public class CommentInfo implements Serializable {

    private static final long serialVersionUID = 1644399052286823386L;

    @Transient
    private String articleId;

    /**
     * 用户id
     */
    private Long userId;

    private String commentId;

    private String headImage;

    private String nickname;

    private String content;

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
