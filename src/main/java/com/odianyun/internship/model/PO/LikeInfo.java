package com.odianyun.internship.model.PO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;

/**
 * @description:
 * @author: EDZ
 * @time: 18:30
 * @date: 2021/7/25
 */
@ApiModel(value = "点赞信息")
public class LikeInfo implements Serializable {
    private static final long serialVersionUID = -2529185535304358958L;

    @ApiModelProperty(value = "文章id", example = "60fe23a9b9e37020247db75b")
    @Transient
    private String articleId;

    @ApiModelProperty(value = "用户id", example = "123")
    private Long userId;

    private String headImage;

    private String nickname;

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
