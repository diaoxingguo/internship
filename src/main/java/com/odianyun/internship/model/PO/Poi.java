package com.odianyun.internship.model.PO;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: EDZ
 * @time: 9:22
 * @date: 2021/7/23
 */
@Document
//@CompoundIndexes({
//        @CompoundIndex(name = "name_point", def = "{'name': 1, 'point': -1}")
//})
public class Poi implements Serializable {
    private static final long serialVersionUID = -2357689916675592111L;

    @Id
    private String id;

//    @Indexed
    private String name;

    private Double point;

//    @Field("stars")
    private Double star;

    private Integer commentNum;

    private String detail;

    private String address;

    private String picture;

    /**
     * 扩展信息
     */
    private Map<String, Object> extInfo;

    /**
     * 评论列表
     */
    private List<CommentInfo> commentInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPoint() {
        return point;
    }

    public void setPoint(Double point) {
        this.point = point;
    }

    public Double getStar() {
        return star;
    }

    public void setStar(Double star) {
        this.star = star;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Map<String, Object> getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(Map<String, Object> extInfo) {
        this.extInfo = extInfo;
    }

    public List<CommentInfo> getCommentInfo() {
        return commentInfo;
    }

    public void setCommentInfo(List<CommentInfo> commentInfo) {
        this.commentInfo = commentInfo;
    }
}
