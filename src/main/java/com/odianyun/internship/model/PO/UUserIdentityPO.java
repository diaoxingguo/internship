package com.odianyun.internship.model.PO;

import java.io.Serializable;
import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 12:24
 * @date: 2021/7/22
 */
public class UUserIdentityPO implements Serializable {

    private Long userId;

    private String extField1;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getExtField1() {
        return extField1;
    }

    public void setExtField1(String extField1) {
        this.extField1 = extField1;
    }
}
