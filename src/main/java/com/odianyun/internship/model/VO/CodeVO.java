package com.odianyun.internship.model.VO;

import java.io.Serializable;

/**
 * @description:
 * @author: EDZ
 * @time: 19:35
 * @date: 2021/7/21
 */
public class CodeVO implements Serializable {
    private static final long serialVersionUID = 4750402797496154986L;

    private String code;

    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
