package com.odianyun.internship.repository;

import com.odianyun.internship.model.PO.Poi;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PoiRepository extends MongoRepository<Poi, String> {
}
