package com.odianyun.internship.service;

import com.odianyun.internship.model.DTO.ArticleDTO;
import com.odianyun.internship.model.PO.Article;
import com.odianyun.internship.model.PO.CommentInfo;
import com.odianyun.internship.model.PO.LikeInfo;

import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 18:55
 * @date: 2021/7/25
 */
public interface ArticleService {
    Article add(ArticleDTO dto);

    Article get(String id);

    String addLike(LikeInfo dto);

    String delete(String id);

    String deleteLikeInfo(LikeInfo dto);

    List<Article> listByTitle(String title);

    String addComment(CommentInfo dto);

    String deleteComment(CommentInfo dto);

    String deleteLike(LikeInfo dto);
}
