package com.odianyun.internship.service;

import com.odianyun.internship.model.PO.Poi;

import java.util.List;

public interface PoiService {
    String save(Poi poi);

    void batchSave(List<Poi> poiList);
}
