package com.odianyun.internship.service;

import com.odianyun.internship.model.DTO.UUserIdentityDTO;

import java.util.List;

public interface UUserIdentityService {
    List<String> listUserLabelById(Long userId);

    void updateUserLabel(UUserIdentityDTO dto);
}
