package com.odianyun.internship.service.impl;

import com.google.common.collect.ImmutableMap;
import com.mongodb.client.result.UpdateResult;
import com.odianyun.internship.constant.CommonConstant;
import com.odianyun.internship.model.DTO.ArticleDTO;
import com.odianyun.internship.model.PO.Article;
import com.odianyun.internship.model.PO.CommentInfo;
import com.odianyun.internship.model.PO.LikeInfo;
import com.odianyun.internship.model.PO.Poi;
import com.odianyun.internship.service.ArticleService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @description:
 * @author: EDZ
 * @time: 18:55
 * @date: 2021/7/25
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * 保存文章
     * @param dto
     * @return
     */
    @Override
    public Article add(ArticleDTO dto) {
        Article article = new Article();
        BeanUtils.copyProperties(dto, article);
        article.setCreateTime(new Date());
        article.setViewNum(0);
        mongoTemplate.insert(article);
        return article;
    }

    @Override
    public String delete(String id) {
        Query query = new Query(Criteria.where("_id").is(id));
        mongoTemplate.remove(query, Article.class);
        return CommonConstant.SUCCESS;
    }

    @Override
    public Article get(String id) {
        // 根据id获取文章信息
        Article article = mongoTemplate.findById(id, Article.class);
        // 浏览量+1
        Query query = new Query(Criteria.where("_id").is(id));
        Update update = new Update();
        update.inc("viewNum");
        mongoTemplate.updateFirst(query, update,Article.class);
        return article;
    }

    @Override
    public String addLike(LikeInfo dto) {
        // 查询是否存在
        Criteria criteria = new Criteria();
        criteria.and("_id").is(dto.getArticleId());
        criteria.and("likeInfo.userId").is(dto.getUserId());
        Query query = new Query(criteria);
        Article article = mongoTemplate.findOne(query, Article.class);
        // 如果存在，返回
        if(null != article) {
            return CommonConstant.SUCCESS;
        }

        Query query2 = new Query(Criteria.where("_id").is(dto.getArticleId()));
        Update update = new Update();
        update.push("likeInfo", dto);
        mongoTemplate.updateFirst(query2, update, Article.class);
        return CommonConstant.SUCCESS;
    }

    @Override
    public String deleteLikeInfo(LikeInfo dto) {
        Query query = new Query(Criteria.where("_id").is(dto.getArticleId()));
        Update update = new Update();
        update.pull("likeInfo", ImmutableMap.of("userId", dto.getUserId()));
        mongoTemplate.updateFirst(query, update, Article.class);
        return CommonConstant.SUCCESS;
    }

    @Override
    public List<Article> listByTitle(String title) {
        Criteria criteria = new Criteria();
        // 模糊查询
        Pattern pattern = Pattern.compile("^.*" + title + ".*$", Pattern.CASE_INSENSITIVE);
        criteria.and("title").regex(pattern);
        Query query = new Query(criteria);
        List<Article> list = mongoTemplate.find(query, Article.class);
        return list;
    }

    @Override
    public String addComment(CommentInfo dto) {
        dto.setCommentId(UUID.randomUUID().toString());
        Query query = new Query(Criteria.where("_id").is(dto.getArticleId()));
        Update update = new Update();
        update.push("commentInfo", dto);
        mongoTemplate.updateFirst(query, update, Article.class);
        return CommonConstant.SUCCESS;
    }

    @Override
    public String deleteComment(CommentInfo dto) {
        Query query = new Query(Criteria.where("_id").is(dto.getArticleId()));
        Update update = new Update();
        update.pull("commentInfo", ImmutableMap.of("commentId", dto.getCommentId()));
        mongoTemplate.updateFirst(query, update, Article.class);
        return CommonConstant.SUCCESS;
    }

    /**
     * 删除点赞
     * @param dto
     * @return
     */
    @Override
    public String deleteLike(LikeInfo dto) {
        /*Criteria criteria = new Criteria();
        criteria.and("_id").is(dto.getArticleId());
        Query query = new Query(criteria);*/
        Query query = new Query(Criteria.where("_id").is(dto.getArticleId()));
        Update update = new Update();
        /*LikeInfo likeInfo = new LikeInfo();
        likeInfo.setUserId(dto.getUserId());
        update.pull("likeInfo", likeInfo);*/
        update.pull("likeInfo", ImmutableMap.of("userId", dto.getUserId()));
        mongoTemplate.updateFirst(query, update, Article.class);
        return CommonConstant.SUCCESS;
    }


}
