package com.odianyun.internship.service.impl;

import com.odianyun.internship.model.PO.Poi;
import com.odianyun.internship.service.PoiService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 9:27
 * @date: 2021/7/23
 */
@Service
public class PoiServiceImpl implements PoiService {

    @Resource
    private MongoTemplate mongoTemplate;


    @Override
    public String save(Poi poi) {
        // 不存在插入，存在时更新
//        mongoTemplate.save(poi);
        // 保存
        mongoTemplate.insert(poi);
        return poi.getId();
    }

    @Override
    public void batchSave(List<Poi> poiList) {
        mongoTemplate.insertAll(poiList);
    }


}
