package com.odianyun.internship.web;

import com.odianyun.internship.constant.CommonConstant;
import com.odianyun.internship.model.DTO.ArticleDTO;
import com.odianyun.internship.model.PO.Article;
import com.odianyun.internship.model.PO.CommentInfo;
import com.odianyun.internship.model.PO.LikeInfo;
import com.odianyun.internship.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 18:13
 * @date: 2021/7/25
 */
@RestController
@RequestMapping("article")
@Api(tags = {"文章接口"})
public class ArticleController {

    @Resource
    private ArticleService articleService;

    @ApiOperation(value = "保存文章", notes = "前端：文章管理-新增文章")
    @PostMapping("add")
    public Article add(@RequestBody ArticleDTO dto) {
        if(null == dto.getUserId() || StringUtils.isBlank(dto.getTitle())
                || StringUtils.isBlank(dto.getContent())) {
            return null;
        }
        return articleService.add(dto);
    }

    @GetMapping("delete")
    public String delete(@RequestParam(value = "id", required = true) String id) {
        return articleService.delete(id);
    }

    @ApiOperation(value = "根据文章id获取文章信息", notes = "前端，文章管理-文章详情")
    @GetMapping("get")
    public Article get(@ApiParam(value = "文章id", example = "60fe23a9b9e37020247db75b", required = true) String id ) {
        return articleService.get(id);
    }

    @ApiIgnore
    @GetMapping("listByTitle")
    public List<Article> listByTitle(@RequestParam(value = "title", required = true)String title) {
        return articleService.listByTitle(title);
    }

    @PostMapping("addLike")
    public String addLike(@RequestBody LikeInfo dto) {
        if(null == dto.getArticleId() || null == dto.getUserId()) {
            return CommonConstant.FAIL;
        }
        return articleService.addLike(dto);
    }

    @PostMapping("deleteLikeInfo")
    public String deleteLikeInfo(@RequestBody LikeInfo dto) {
        if(null == dto.getArticleId() || null == dto.getUserId()) {
            return CommonConstant.FAIL;
        }
        return articleService.deleteLikeInfo(dto);
    }

    @PostMapping("deleteLike")
    public String deleteLike(@RequestBody LikeInfo dto) {
        if(null == dto.getArticleId() || null == dto.getUserId()) {
            return CommonConstant.FAIL;
        }

        return articleService.deleteLike(dto);
    }

    @PostMapping("addComment")
    public String addComment(@RequestBody CommentInfo dto) {
        if(null == dto.getArticleId() || null == dto.getUserId()) {
            return CommonConstant.FAIL;
        }
        return articleService.addComment(dto);
    }

    @PostMapping("deleteComment")
    public String deleteComment(@RequestBody CommentInfo dto) {
        if(null == dto.getArticleId() || null == dto.getCommentId()) {
            return CommonConstant.FAIL;
        }
        return articleService.deleteComment(dto);
    }
}
