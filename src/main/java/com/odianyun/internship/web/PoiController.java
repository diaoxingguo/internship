package com.odianyun.internship.web;

import com.odianyun.internship.model.PO.Poi;
import com.odianyun.internship.repository.PoiRepository;
import com.odianyun.internship.service.PoiService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 9:26
 * @date: 2021/7/23
 */
@RestController
@RequestMapping("poi")
public class PoiController {

    @Resource
    private PoiService poiService;

    @Resource
    private PoiRepository poiRepository;

    @PostMapping("save")
    public String save(@RequestBody Poi poi) {
        return poiService.save(poi);
    }

    @PostMapping("insert")
    public Poi insert(@RequestBody Poi poi) {
        return poiRepository.insert(poi);
    }

    @PostMapping("batchSave")
    public void batchSave(@RequestBody List<Poi> poiList) {
        poiService.batchSave(poiList);
    }
}
