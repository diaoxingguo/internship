package com.odianyun.internship.web;

import com.odianyun.internship.model.DTO.CodeDTO;
import com.odianyun.internship.model.DTO.UUserIdentityDTO;
import com.odianyun.internship.model.VO.CodeVO;
import com.odianyun.internship.service.CodeService;
import com.odianyun.internship.service.UUserIdentityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @description:
 * @author: EDZ
 * @time: 19:34
 * @date: 2021/7/21
 */
@RestController
@RequestMapping("uUserIdentity")
public class UUserIdentityController {

    @Resource
    private UUserIdentityService uUserIdentityService;

    @GetMapping("listUserLabelById")
    public List<String> listUserLabelById(@RequestParam(name = "userId", required = true) Long userId) {
        return uUserIdentityService.listUserLabelById(userId);
    }

    @PostMapping("updateUserLabel")
    public void updateUserLabel(@RequestBody UUserIdentityDTO dto) {
        if (null == dto.getUserId() || null == dto.getExtField1List()) {
            return;
        }
        uUserIdentityService.updateUserLabel(dto);
    }
}
