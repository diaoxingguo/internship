package com.odianyun.internship;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.odianyun.internship.model.PO.CommentInfo;
import com.odianyun.internship.model.PO.Poi;
import com.odianyun.internship.repository.PoiRepository;
import com.odianyun.internship.startup.Application;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicUpdate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * @description:
 * @author: EDZ
 * @time: 9:56
 * @date: 2021/7/23
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MongodbTest {

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private PoiRepository poiRepository;

    @Test
    public void testFindById() {
        String id = "60fa1ffeb9e3708dc84aae98";
        Poi poi = mongoTemplate.findById(id, Poi.class);
        System.out.println("poi == " + JSONObject.toJSONString(poi));
    }

    @Test
    public void testFindAll() {
        List<Poi> poiList = mongoTemplate.findAll(Poi.class);
        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
    }

    @Test
    public void testFindWithWhere() {
        // Criteria.where
        Query query = new Query(Criteria.where("_id").is("60fa1ffeb9e3708dc84aae98"));
        List<Poi> poiList = mongoTemplate.find(query, Poi.class);

        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("count == " + poiList.size());
    }

    @Test
    public void testFindWithAnd() {

        Criteria criteria = new Criteria();
        // and is
//        criteria.and("id").is("60fa1ffeb9e3708dc84aae98");

        // and
        /*criteria.and("name").is("狮子林");
        criteria.and("type").is("景点");*/

        criteria.and("extInfo.mobile").is("0512-62986008");
        Query query = new Query(criteria);
        List<Poi> poiList = mongoTemplate.find(query, Poi.class);

        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("count == " + poiList.size());
    }

    @Test
    public void testFindWithOr() {
        Criteria criteria = new Criteria();

        // or
        criteria.orOperator(Criteria.where("name").is("狮子林"), Criteria.where("type").is("景点"));
        Query query = new Query(criteria);

        List<Poi> poiList = mongoTemplate.find(query, Poi.class);
        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("count == " + poiList.size());
    }

    @Test
    public void testFindWithIn() {
        Criteria criteria = new Criteria();
        // in
        criteria.and("type").in(Lists.newArrayList("景点", "餐厅"));
        Query query = new Query(criteria);

        List<Poi> poiList = mongoTemplate.find(query, Poi.class);
        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("count == " + poiList.size());
    }

    @Test
    public void testFindWithNull() {
        Criteria criteria = new Criteria();
        // 值为null
        criteria.and("type").is(null);
        Query query = new Query(criteria);

        List<Poi> poiList = mongoTemplate.find(query, Poi.class);
        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("count == " + poiList.size());
    }

    @Test
    public void testFindWithRegex() {
        Criteria criteria = new Criteria();

        // 模糊查询
        String patternName = "金";
        Pattern pattern = Pattern.compile("^.*" + patternName + ".*$", Pattern.CASE_INSENSITIVE);
        criteria.and("name").regex(pattern);
        Query query = new Query(criteria);

        List<Poi> poiList = mongoTemplate.find(query, Poi.class);
        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("count == " + poiList.size());
    }

    @Test
    public void testFindWithLimit() {
        // 排序
        Query query = new Query().with(new Sort(Sort.Direction.DESC, "commentNum"));
        // 分页
        query.skip(0).limit(10);

        // 获取总数
        long total = mongoTemplate.count(query, Poi.class);
        List<Poi> poiList = mongoTemplate.find(query, Poi.class);

        System.out.println("poiList == " + JSONObject.toJSONString(poiList));
        System.out.println("size == " + poiList.size());
        System.out.println("total == " + total);
    }

    @Test
    public void testUpdateMulti() {
        Query query = new Query(Criteria.where("name").is("狮子林"));
        Update update = new Update();
        update.set("point", 8.4);
        // 全部更新
        mongoTemplate.updateMulti(query, update, Poi.class);
    }

    @Test
    public void testUpdateFirst() {
        Query query = new Query(Criteria.where("name").is("狮子林"));
        Update update = new Update();
        update.set("point", 8.5);
        // // 更新第一个
        mongoTemplate.updateFirst(query, update, Poi.class);
    }

    @Test
    public void testUpsert() {
        Query query = new Query(Criteria.where("name").is("狮子林2"));
        Update update = new Update();
        update.set("point", 8.6);
        // 存在更新，不存在插入
        mongoTemplate.upsert(query, update, Poi.class);
    }


    @Test
    public void testRemove() {
        Query query = new Query(Criteria.where("name").is("狮子林2"));
        // 删除
        DeleteResult deleteResult = mongoTemplate.remove(query, Poi.class);
        System.out.println("deletedCount == " + deleteResult.getDeletedCount());

    }

    @Test
    public void testPush() {
        Query query = new Query(Criteria.where("_id").is("60fa7df8b9e37019bcfd1394"));

        Update update = new Update();
        CommentInfo info = new CommentInfo();
        info.setCommentId("13802985");
        info.setContent("这里很漂亮");
        info.setUserId(1202038L);
        info.setHeadImage("https://pic.qyer.com/avatar/2.png?v=1622032913");
        info.setNickname("懒人语娟");
        update.push("commentInfo", info);
        // 数组添加数据
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Poi.class);
        System.out.println("modifiedCount == " + updateResult.getModifiedCount());
    }

    @Test
    public void testPull() {
        Query query = new Query(Criteria.where("_id").is("60fa7df8b9e37019bcfd1394"));

        Update update = new Update();
        CommentInfo info = new CommentInfo();
        info.setCommentId("13802985");
        update.pull("commentInfo", info);

//        Update update = new BasicUpdate("{'$pull': {'commentInfo': {'commentId': 13802985L}}}");
        // 数组移除数据
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Poi.class);
        System.out.println("modifiedCount == " + updateResult.getModifiedCount());
    }

    @Test
    public void testInc() {
        Query query = new Query(Criteria.where("_id").is("60fa7df8b9e37019bcfd1394"));
        Update update = new Update();
        // 自增1
//        update.inc("commentNum");
        // 增加10
//        update.inc("commentNum", 10);
        // 负数表示减
        update.inc("commentNum", -5);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Poi.class);
        System.out.println("modifiedCount == " + updateResult.getModifiedCount());
    }


    @Test
    public void testMongoRepositoryInsert() {
        Poi poi = new Poi();
        poi.setName("沧浪亭");
        poi.setDetail("沧浪亭，世界文化遗产，位于苏州市城南三元坊附近，在苏州现存诸园中历史最为悠久。始建于北宋，为文人苏舜钦的私人花园，称“沧浪亭”。沧浪亭占地面积1.08公顷。园内有一泓清水贯穿，波光倒影，景象万千。");
        poiRepository.insert(poi);

        System.out.println("poi == " + JSONObject.toJSONString(poi));
    }

    @Test
    public void testMongoRepositoryFindById() {
        Optional<Poi> optionalPoi = poiRepository.findById("60fa84b6b9e370ba045822db");
        if (optionalPoi.isPresent()) {
            System.out.println("poi == " + JSONObject.toJSONString(optionalPoi.get()));
        }
    }

    @Test
    public void testMongoRepositoryFindAll() {
        Poi poi = new Poi();
        poi.setName("寒山寺");
        // 条件
        Example example = Example.of(poi);
        // 分页
        Pageable pageable = PageRequest.of(0, 3);

        Page<Poi> pageList = poiRepository.findAll(example, pageable);

        long count = poiRepository.count(example);

        System.out.println("poi == " + JSONObject.toJSONString(pageList.getContent()));
        System.out.println("size == " + pageList.getSize());
        System.out.println("count == " + count);
    }


    @Test
    public void testMongoRepositoryDelete() {
        poiRepository.deleteById("60fa3061b9e370ab741c95c2");
    }
}
