package com.odianyun.internship;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.odianyun.internship.model.UUser;
import com.odianyun.internship.startup.Application;
import com.sun.org.apache.xpath.internal.operations.Bool;
import jdk.management.resource.internal.inst.FileOutputStreamRMHooks;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.*;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: EDZ
 * @time: 14:16
 * @date: 2021/7/20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class RedisTest {

    @Resource
    private RedisTemplate<String, Serializable> redisTemplate;

    @Test
    public void testStringSet() {
        ValueOperations operations = redisTemplate.opsForValue();
        // 设值
        String key = "userName:2";
        operations.set(key, "xingguo");
        // 使用最后一个
        operations.set(key, "11");
        // 获取值
        System.out.println("value = " + operations.get(key));
        System.out.println(operations.size(key));
    }

    @Test
    public void testStringExpire() throws InterruptedException {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "userCaptchas:1";
        // 设置过期时间
        operations.set(key, "435678", 3, TimeUnit.SECONDS);
        System.out.println("value = " + operations.get(key));
        Thread.sleep(4000);
        System.out.println("4s之后value = " + operations.get(key));
    }

    @Test
    public void testStringSetIfAbsent() throws InterruptedException {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "userName:2";
        String key3 = "userName:3";
        String key6 = "userName:6";
        // 如果不存在，则设置一个key和value。如果存在则不改变
        operations.setIfAbsent(key, "diaoxingguo2");
        operations.setIfAbsent(key3, "diaoxingguo3");
        operations.setIfAbsent(key6, "diaoxingguo6");
        System.out.println("key value = " + operations.get(key));
        System.out.println("key3 value = " + operations.get(key3));
        System.out.println("key6 value = " + operations.get(key6));
    }

    @Test
    public void testStringSetIfPresent() throws InterruptedException {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "userName:2";
        String key3 = "userName:3";
        String key8 = "userName:8";
        // 如果key存在就使用新值覆盖，如果key不存在，则不赋值
        operations.setIfPresent(key, "diaoxingguo2");
        operations.setIfPresent(key3, "diaoxingguo3");
        operations.setIfPresent(key8, "diaoxingguo8");
        System.out.println("key value = " + operations.get(key));
        System.out.println("key3 value = " + operations.get(key3));
        System.out.println("key8 value = " + operations.get(key8));
    }

    @Test
    public void testStringMultiSet() {
        ValueOperations operations = redisTemplate.opsForValue();
//        Map<String, String> map = new HashMap<>();
//        Map<String, String> map = Maps.newHashMap();
//        map.put("", "");
        // 批量设置key和value
        Map<String, String> map = ImmutableMap.of(
                "userName:101", "张三",
                "userName:102", "李四",
                "userName:103", "刘汝清"
        );
        operations.multiSet(map);
        // 获取单个key
        System.out.println("userName:101 value = " + operations.get("userName:101"));

        // 获取批量key的值
        List<String> multiValues = operations.multiGet(Lists.newArrayList("userName:101", "userName:102", "userName:103"));

        multiValues.forEach(System.out::println);

        /*multiValues.forEach(item -> System.out.println(item));

        for(String item : multiValues) {
            System.out.println(item);
        }*/

        List<String> filterList = multiValues.stream().filter(item -> !item.equals("张三")).collect(Collectors.toList());
        System.out.println("filterList = " + JSONObject.toJSONString(filterList));

        /*List<String> filterList = Lists.newArrayList();
        for(String item : multiValues) {
            if(!item.equals("张三")) {
                filterList.add(item);
            }
        }*/
    }

    @Test
    public void testStringIncrement() {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "url:query:num";
        Long maxNum = 10L;
        Long num = 0L;
        operations.set(key, 0);
        while (maxNum.compareTo(num) > 0) {
            // 加值
            num = operations.increment(key);
            System.out.println("num = " + num);
        }
    }

    @Test
    public void testStringIncrementWithDelta() {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "url:query:num";
        Long maxNum = 10L;
        Long num = 0L;
        Long delta = 2L;
        operations.set(key, 0);
        while (maxNum.compareTo(num) > 0) {
            // 加值
            num = operations.increment(key, delta);
            System.out.println("num = " + num);
        }
    }

    @Test
    public void testStringDecrement() {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "url:query:num";
        Long minNum = 0L;
        Long num = 10L;
        Long delta = 2L;
        operations.set(key, num);
        while (num.compareTo(minNum) > 0) {
            // 减值
            num = operations.decrement(key, delta);
            System.out.println("num = " + num);
        }
    }

    @Test
    public void testStringGetAndSet() {
        ValueOperations operations = redisTemplate.opsForValue();
        String key = "url:query:num";
        String key3 = "url:query:num3";
        // 设置新值返回旧值，如果key不存在，旧值就是null
        /*System.out.println("getAndSet key value = " + operations.get(key));
        System.out.println("getAndSet key value = " + operations.getAndSet(key, 1000));
        System.out.println("getAndSet key value = " + operations.get(key));

        System.out.println("getAndSet key value = " + operations.get(key3));
        System.out.println("getAndSet key value = " + operations.getAndSet(key3, 1000));
        System.out.println("getAndSet key value = " + operations.get(key3));*/

        System.out.println("key3 value = " + operations.get(key3));
        // 判断key是否存在
        System.out.println("hasKey key3 = " + redisTemplate.hasKey(key3));
        // 删除key
        redisTemplate.delete(key3);
        System.out.println("key3 value = " + operations.get(key3));
        System.out.println("hasKey key3 = " + redisTemplate.hasKey(key3));

        System.out.println(JSONObject.toJSONString(operations.multiGet(Lists.newArrayList(key, key3))));
        redisTemplate.delete(Lists.newArrayList(key, key3));
        System.out.println(JSONObject.toJSONString(operations.multiGet(Lists.newArrayList(key, key3))));
    }

    @Test
    public void testObject() {
        ValueOperations<String, Serializable> operations = redisTemplate.opsForValue();
        String key = "userInfo:123";
        UUser user = new UUser();
        user.setId(123L);
        user.setMobile("13127787865");
        user.setPassword("123456");

        // 存放对象
        operations.set(key, user);

        UUser cacheUUser = (UUser) operations.get(key);

        System.out.println(JSONObject.toJSONString(cacheUUser));
    }

    /**
     * Long leftPush(K key, V value);
     * 左边添加，list头部
     */
    @Test
    public void testListPush() {
        ListOperations<String, Serializable> operations =redisTemplate.opsForList();
        String key = "user:fans:1";

        // list头部
        /*operations.leftPush(key, "刘德华");
        operations.leftPush(key, "张学友");
        operations.leftPush(key, "郭富城");
        operations.leftPush(key, "黎明");
        operations.leftPush(key, "周杰伦");*/

        // list尾部
        operations.rightPush(key, "王力宏");

        // value放在指定值pivot的前面
        operations.leftPush(key,"黎明", "刘汝清");

        // value放在指定值pivot的后面
        operations.rightPush(key, "周杰伦", "兰龙雨");
        // 获取list的范围值 start, end是索引。
        // end是-1表示获取全部
        List<Serializable> values = operations.range(key, 0, -1);

        System.out.println("values == " + JSONObject.toJSONString(values));
    }


    @Test
    public void testListPushAll() {
        ListOperations<String, Serializable> operations =redisTemplate.opsForList();
        String key = "user:fans:2";

        // 删除
        redisTemplate.delete(key);
        // 左push
        operations.leftPushAll(key, "刘德华", "张学友");
        operations.leftPushAll(key, Lists.newArrayList("郭富城", "黎明"));

        // 右push
        operations.rightPushAll(key, "周杰伦", "王力宏");
        operations.rightPushAll(key, Lists.newArrayList("赵雷", "李荣浩"));

        List<Serializable> values = operations.range(key, 0, -1);

        System.out.println("values == " + JSONObject.toJSONString(values));
    }

    @Test
    public void testListPushIfPresent() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();

        String key = "user:fans:3";
        String key2 = "user:fans:2";

        /**
         * leftPushIfPresent当key有值的时候，左边添加。否则不做任何操作
         */
        operations.leftPushIfPresent(key, "周杰伦");
        operations.leftPushIfPresent(key, "王力宏");
        operations.leftPushIfPresent(key, "王力宏");

        operations.leftPushIfPresent(key2, "周杰伦");
        operations.leftPushIfPresent(key2, "王力宏");
        operations.leftPushIfPresent(key2, "王力宏");

        // rightPushIfPresent当key有值的时候，左边添加。否则不做任何操作
        operations.rightPushIfPresent(key2, "王力宏");

        List<Serializable> values = operations.range(key, 0, -1);
        System.out.println("key values == " + JSONObject.toJSONString(values));
        // key2
        System.out.println("key2 values == " + JSONObject.toJSONString(operations.range(key2, 0, -1)));
    }

    @Test
    public void testListPop() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();
        String key = "user:fans:2";

        System.out.println("old values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));

        // 左删除
        String leftValue = (String) operations.leftPop(key);

        // 右删除
        String rightValue = (String) operations.rightPop(key);

        System.out.println("left value == " + leftValue);

        System.out.println("right value == " + rightValue);

        System.out.println("current values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
    }

    @Test
    public void testListRemove() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();
        String key = "user:fans:2";

        System.out.println("old values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));

        operations.leftPush(key, "张学友");

        System.out.println("old values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
        // 指定个数count删除与value的值
        Long successNum = operations.remove(key, 100, "张学友");

        System.out.println("successNum == " + successNum);
        System.out.println("current values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
    }

    @Test
    public void testListRightPopAndLeftPush() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();
        String key = "user:fans:2";
        String destinationKey = "user:fans:5";

        System.out.println("key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
        System.out.println("destinationKey values == " + JSONObject.toJSONString(operations.range(destinationKey, 0, -1)));

        // 从源list删除，放入目标list的左边
        operations.rightPopAndLeftPush(key, destinationKey);

        System.out.println("current key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
        System.out.println("current destinationKey values == " + JSONObject.toJSONString(operations.range(destinationKey, 0, -1)));
    }

    @Test
    public void testListRightPopAndLeftPushWithTimeout() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();
        String key = "user:fans:6";
        String destinationKey = "user:fans:5";

        // 先删除，以防有值
        redisTemplate.delete(key);

        System.out.println("key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
        System.out.println("destinationKey values == " + JSONObject.toJSONString(operations.range(destinationKey, 0, -1)));

        Long size = operations.size(destinationKey);
        System.out.println("size == " + size);

        Long start = System.currentTimeMillis();
        // 从源list删除，放入目标list的左边
        operations.rightPopAndLeftPush(key, destinationKey, 2, TimeUnit.SECONDS);

        Long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start)/1000 + "s");

        System.out.println("current key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
        System.out.println("current destinationKey values == " + JSONObject.toJSONString(operations.range(destinationKey, 0, -1)));
    }

    @Test
    public void testListSet() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();
        String key = "user:fans:5";

        System.out.println("key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));

        // 修改指定下标的值，如果下标超过list范围会报错
        operations.set(key, 1, "张学友");
//        operations.set(key, 3, "张学友");

        System.out.println("key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
    }

    @Test
    public void testListTrim() {
        ListOperations<String, Serializable> operations = redisTemplate.opsForList();
        String key = "user:fans:5";

        System.out.println("key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));

        // 保留下标从start到end的值
        operations.trim(key, 0, 1);

        // 指定key过期时间
        redisTemplate.expire(key, 2, TimeUnit.SECONDS);

        System.out.println("key values == " + JSONObject.toJSONString(operations.range(key, 0, -1)));
    }


    @Test
    public void testHashPut() {
        HashOperations<String, String, Serializable> operations = redisTemplate.opsForHash();
        String key = "userInfo:1";

        String hashKeyMobile = "mobile";
        String hashKeyPassword = "password";

        // 设值
        operations.put(key, hashKeyMobile, "13127787865");
        operations.put(key, hashKeyPassword, "123456");

        // 根据key和hashKey获取值
        System.out.println("mobile == " + operations.get(key, hashKeyMobile));
        System.out.println("password == " + operations.get(key, hashKeyPassword));

        // 根据key和hashKey的集合获取值
        List<Serializable> listValues = operations.multiGet(key, Lists.newArrayList(hashKeyMobile, hashKeyPassword));

        System.out.println("listValues == " + JSONObject.toJSONString(listValues));

    }

    @Test
    public void testHashPutAll() {
        HashOperations<String, String, Serializable> operations = redisTemplate.opsForHash();
        String key = "userInfo:2";

        String key3 = "userInfo:3";

        String hashKeyMobile = "mobile";
        String hashKeyPassword = "password";
        String hashKeyEmail = "email";

        Map<String, Serializable> map = ImmutableMap.of(
                hashKeyMobile, "13127787865",
                hashKeyPassword, "123456"
        );

        // 设值map
        operations.putAll(key, map);

        // 如果key值存在，在key中hashKey也存在，不做处理，
        // 如果key值存在，hashKey不存在，在key中可以添加不存在的的键值对，
        operations.putIfAbsent(key, hashKeyMobile, "13127787866");
        operations.putIfAbsent(key, hashKeyEmail, "diaoxingguo@odianyun.com");
        // 如果key不存在，则新增一个key，同时将键值对hashKey和hashValue添加到该key中。
        operations.putIfAbsent(key3, hashKeyMobile, "13127787866");

        // 根据key和hashKey获取值
        System.out.println("mobile == " + operations.get(key, hashKeyMobile));
        System.out.println("password == " + operations.get(key, hashKeyPassword));
        System.out.println("email == " + operations.get(key, hashKeyEmail));

        // 根据key和hashKey的集合获取值
        List<Serializable> listValues = operations.multiGet(key, Lists.newArrayList(hashKeyMobile, hashKeyPassword, hashKeyEmail));

        System.out.println("listValues == " + JSONObject.toJSONString(listValues));

        // operations.entries获取hashKey和hashValue
        Map<String, Serializable> hashKeyValueMap = operations.entries(key);
        Map<String, Serializable> hashKeyValueMap3 = operations.entries(key3);

        System.out.println("hashKeyValueMap == " + JSONObject.toJSONString(hashKeyValueMap));

        System.out.println("hashKeyValueMap3 == " + JSONObject.toJSONString(hashKeyValueMap3));
    }

    @Test
    public void testHashHasKey() {
        HashOperations<String, String, Serializable> operations = redisTemplate.opsForHash();
        String key = "userInfo:2";
        String hashKeyMobile = "mobile";
        String hashKeyPassword = "password";
        String hashKeyEmail = "email";

        // 是否有hashKey，返回true或者false
        Boolean hasKey = operations.hasKey(key, hashKeyMobile);
        System.out.println("hasKey == " + hasKey);

        System.out.println("hashKeyValueMap == " + JSONObject.toJSONString(operations.entries(key)));

        // 删除hashKey,返回成功的个数
        Long deleteNum = operations.delete(key, hashKeyMobile, "nickname");

        System.out.println("deleteNum == " + deleteNum);
        System.out.println("hasKey == " + operations.hasKey(key, hashKeyMobile));
        System.out.println("hashKeyValueMap == " + JSONObject.toJSONString(operations.entries(key)));

    }

    @Test
    public void testHashKeys() {
        HashOperations<String, String, Serializable> operations = redisTemplate.opsForHash();
        String key = "userInfo:2";

        // 获取key中hashKey的集合
        Set<String> hashKeySet = operations.keys(key);

        System.out.println("hashKeySet == " + JSONObject.toJSONString(hashKeySet));

        // 获取key中hashValue的集合
        List<Serializable> hashValueList = operations.values(key);

        System.out.println("hashValueList == " + JSONObject.toJSONString(hashValueList));

        // 获取key中hashKey的个数
        Long hashKeySize = operations.size(key);

        System.out.println("hashKeySize == " + JSONObject.toJSONString(hashKeySize));
    }

    @Test
    public void testHashIncrement() {
        HashOperations<String, String, Integer> operations = redisTemplate.opsForHash();
        String key = "user:visit:3";
        for (int i=0; i<10; i++){
            System.out.println("result score == " + operations.increment(key, "pv", 2));
        }
    }

    @Test
    public void testSetAdd() {
        SetOperations<String, Serializable> operations = redisTemplate.opsForSet();
        String key = "user:label:1";
        // 添加单个值
        operations.add(key, "帅气");
        operations.add(key, "大方");

        // 添加多个值
        operations.add(key, "帅气", "善良", "高", "富");

        // 获取set中的值集合
        Set<Serializable> values = operations.members(key);
        System.out.println("values == " + values);

        System.out.println("是否含有：富 == " + operations.isMember(key, "富"));
        System.out.println("是否含有：穷 == " + operations.isMember(key, "穷"));

    }


    @Test
    public void testSetRandom() {
        SetOperations<String, Serializable> operations = redisTemplate.opsForSet();
        String key = "user:label:1";

        // 按照count个数随机返回，有可能有重复值
        List<Serializable> randomList = operations.randomMembers(key, 3);

        System.out.println("randomList == " + JSONObject.toJSONString(randomList));

        // 按照count个数随机返回，没有重复值
        Set<Serializable> distinctRandomSet = operations.distinctRandomMembers(key, 3);

        System.out.println("distinctRandomSet == " + JSONObject.toJSONString(distinctRandomSet));

        // 随机返回一个值
        String randomValue = (String) operations.randomMember(key);

        System.out.println("randomValue == " + randomValue);

        // 获取key对应值的个数
        System.out.println("size == " + operations.size(key));
    }

    @Test
    public void testSetMove() {
        SetOperations<String, Serializable> operations = redisTemplate.opsForSet();
        String key = "user:label:1";
        String key2 = "user:label:2";

        System.out.println("values == " + operations.members(key));
        // 移除给定的值，返回成功的个数
        Long successNum = operations.remove(key, "高", "穷");

        System.out.println("successNum == " + successNum);
        System.out.println("values == " + operations.members(key));


        operations.move(key, "富", key2);

        System.out.println("values == " + operations.members(key));
        System.out.println("values == " + operations.members(key2));
    }

    @Test
    public void testSetIntersect() {
        SetOperations<String, Serializable> operations = redisTemplate.opsForSet();
        String key = "user:label:3";
        String key2 = "user:label:4";
        String key3 = "user:label:5";
        String key4 = "user:label:6";
        String key5 = "user:label:7";

        operations.add(key, "帅", "善良", "高", "富");
        operations.add(key2, "帅", "善良", "高", "富", "才华", "白");

        System.out.println("values == " + operations.members(key));
        System.out.println("values == " + operations.members(key2));

        // 取交集并返回
        Set<Serializable> intersectSet = operations.intersect(key, key2);

        // 取key和key2的交集赋给key3
        operations.intersectAndStore(key, key2, key3);

        // 取key和key2的并集并返回
        Set<Serializable> unionSet = operations.union(key, key2);

        // 取key和key2的并集赋给key3
        operations.unionAndStore(key, key2, key3);

        // key对key2求差赋给key4
        operations.differenceAndStore(key, key2, key4);
        // key2对key求差赋给key5
        operations.differenceAndStore(key2, key, key5);

        System.out.println("intersectSet == " + JSONObject.toJSONString(intersectSet));
        System.out.println("unionSet == " + JSONObject.toJSONString(unionSet));
        System.out.println("key3 == " + JSONObject.toJSONString(operations.members(key3)));
        System.out.println("key4 == " + JSONObject.toJSONString(operations.members(key4)));
        System.out.println("key5 == " + JSONObject.toJSONString(operations.members(key5)));
    }

    @Test
    public void testZSetAdd() {
        ZSetOperations<String, Serializable> operations =redisTemplate.opsForZSet();
        String key = "weibo:hot";
        // 添加值
        operations.add(key, "郑州", 100);
        operations.add(key, "开封", 90);
        operations.add(key, "登封", 95);
        operations.add(key, "巩义", 85);
        operations.add(key, "中牟", 89);

        // 获取值的范围，end = -1表示全部,从小到大
        Set<Serializable> valueSet = operations.range(key, 0, -1);
        System.out.println("valueSet == " + valueSet);

        // 获取值的范围，end = -1表示全部,从大到小
        Set<Serializable> valueReverseSet = operations.reverseRange(key, 0, -1);
        System.out.println("valueReverseSet == " + valueReverseSet);

        // 获取分数从min到max之间,从小到大
        System.out.println("rangeByScore == " + operations.rangeByScore(key, 90, 100));
        // 获取分数从min到max之间,从大到小
        System.out.println("reverseRangeByScore" + operations.reverseRangeByScore(key, 90, 100));

        // 获取下标，排序从小到大
        System.out.println("rank == " + operations.rank(key, "开封"));
        // 获取下标，排序从大到小
        System.out.println("reverseRank == " + operations.reverseRank(key, "开封"));
        // 获取分数从min到max的总数
        System.out.println("count == " + operations.count(key,90, 100));

    }

    @Test
    public void testHostSearch() {
        ZSetOperations<String, Serializable> operations =redisTemplate.opsForZSet();
        String key = "weibo:hotSearch";
        // 添加值
        operations.add(key, "三个月大婴儿被埋废墟一天一夜获救", 100);
        operations.add(key, "这就是中国人的团结", 95);
        operations.add(key, "河南暴雨互助", 90);
        operations.add(key, "郑州暴雨后人间百态", 85);
        operations.add(key, "辉县卫辉等58个乡镇受灾", 80);
        operations.add(key, "中国法庭", 75);
        operations.add(key, "河南加油河南挺住", 70);
        operations.add(key, "郑州高铁站希岸酒店涨价到2888", 65);
        operations.add(key, "河南为啥三天下了一整年雨", 60);
        operations.add(key, "河南暴雨下的中国人", 50);

        System.out.println("从低到高 == " + operations.range(key, 0 ,9));
        System.out.println("从高到低 == " + operations.reverseRange(key, 0 ,9));

        operations.remove(key, "郑州高铁站希岸酒店涨价到2888");

        System.out.println("从高到低 == " + operations.reverseRange(key, 0 ,9));

        operations.incrementScore(key, "河南暴雨互助", 30);

        System.out.println("从高到低 == " + operations.reverseRange(key, 0 ,9));
    }

}
